import {
    IonButtons,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonContent,
    IonHeader,
    IonItem,
    IonLabel,
    IonList,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonInput,
    IonButton,
    IonTextarea,
    IonCardContent,
    IonIcon
} from '@ionic/react';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom'
import './Home.css';
import { getUser } from '../services/authService';
import { getRecent, likePost, unLikePost, addPost } from '../services/postService';
import { FaRegHeart, FaHeart } from 'react-icons/fa';

class Main extends React.Component<RouteComponentProps> {
    state = {
        posts: [] as any,
        hasMoreItems: true,
        nextParams: {
            take: 10,
            skip: 0
        },
        user: { acessToken: '' },
        postContent: ''
    };
    initialLoadCompleted = false;

    componentDidMount() {
        var user = getUser();
        this.setState({ user: user });
    }

    fetchPosts = async () => {
        var { nextParams, user } = this.state;
        var { data } = await getRecent(nextParams.skip, nextParams.take, user.acessToken);
        this.setState(
            {
                hasMoreItems: data.morePostsExist,
                posts: [...this.state.posts, ...data.posts],
                nextParams: {
                    skip: this.state.nextParams.skip + 5,
                    take: 5
                }
            });
    }

    addPost = async () => {
        var content = this.state.postContent;
        var { data } = await addPost(content, this.state.user.acessToken)
        this.setState({ posts: [data, ...this.state.posts] });
    }

    handleChange = e => {
        var { postContent } = this.state;
        postContent = e.currentTarget.value;
        this.setState({ postContent: postContent });
    }

    handleScroll = async e => {
        var el = await e.currentTarget.getScrollElement();

        var hitTheBottom = el.scrollHeight - el.scrollTop == el.clientHeight;
        if (hitTheBottom) {
            await this.fetchPosts();
            this.setState({ tracks: [...this.state.posts, 1], postContent: '' })
        }
    }

    profileRedirect = (userId) => {
        this.props.history.push('profile/' + userId);
    }

    updatePostsState = (postId, likeStatus) => {
        var { posts } = this.state;

        var postIndex = posts.findIndex(x => x.postId === postId);

        var postsBefore = posts.slice(0, postIndex);
        var postsAfter = posts.slice(postIndex + 1, posts.length)

        var post = posts[postIndex];

        if (likeStatus) {
            post.likeCount++;
        } else {
            post.likeCount--;
        }

        post.userLikesPost = likeStatus;

        this.setState({ posts: [...postsBefore, post, ...postsAfter] });
    }
    likePost = async postId => {
        await likePost(postId, this.state.user.acessToken);
        this.updatePostsState(postId, true);
    }

    unlikePost = async postId => {
        await unLikePost(postId, this.state.user.acessToken);
        this.updatePostsState(postId, false);
    }

    render() {
        if (this.state.user.acessToken !== '' && !this.initialLoadCompleted) {
            this.initialLoadCompleted = true;
            this.fetchPosts();
        }
        var items = [] as any;
        this.state.posts.map((item, i) => {
            items.push(<IonCard key={i}>
                <IonItem>
                    <IonLabel>{item.authorName}</IonLabel>
                    <IonButton onClick={() => this.profileRedirect(item.authorId)} fill="outline" slot="end">Profile</IonButton>
                </IonItem>
                <IonCardContent>
                    {item.content}
                </IonCardContent>
                <div className="display-flex justifyEnd">
                    {item.userLikesPost
                        ? <FaHeart size={20} color={'red'} onClick={() => this.unlikePost(item.postId)}></FaHeart>
                        : <FaRegHeart onClick={() => this.likePost(item.postId)} size={20}></FaRegHeart>
                    }
                    <div>{item.likeCount}</div>
                </div>
            </IonCard>);
        })

        return (
            <div>
                <IonPage >
                    <IonHeader>
                        <IonToolbar>
                            <IonButtons slot="start">
                                <IonMenuButton />
                            </IonButtons>
                            <IonTitle>Tsm</IonTitle>
                        </IonToolbar>
                    </IonHeader>
                    <IonContent scrollEvents={true} onIonScrollEnd={this.handleScroll}>
                        <IonCard className="welcome-card">
                            <img src="/assets/shapes.svg" alt="" />
                            <IonCardHeader>
                                <div className="centerD">
                                    <IonCardTitle>Welcome to TSM</IonCardTitle>
                                </div>
                            </IonCardHeader>
                        </IonCard>
                        <IonItem lines={'none'} >

                        </IonItem>
                        <IonCard className="welcome-card">
                            <IonCardHeader>
                                <div className="centerD">
                                    <IonCardTitle>Add Post</IonCardTitle>
                                </div>
                            </IonCardHeader>
                            <IonCardContent>
                                <IonTextarea
                                    onIonChange={this.handleChange}
                                    autoGrow={true}
                                    value={this.state.postContent}
                                    placeholder="What's on your mind?"></IonTextarea>
                                <div className={'justifyEnd display-flex '}>
                                    <IonButton onClick={this.addPost} color="tertiary">Post</IonButton>
                                </div>
                            </IonCardContent>
                        </IonCard>

                        {items}
                    </IonContent>

                </IonPage>
            </div >

        );
    }
};

export default Main;