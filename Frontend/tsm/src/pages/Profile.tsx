import {
    IonButtons,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonContent,
    IonHeader,
    IonItem,
    IonLabel,
    IonList,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonInput,
    IonButton,
    IonTextarea,
    IonCardContent,
    IonIcon
} from '@ionic/react';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom'
import './Home.css';
import { getUser } from '../services/authService';
import { getUserPosts, likePost, unLikePost } from '../services/postService';
import { FaRegHeart, FaHeart } from 'react-icons/fa';
import { getProfile } from '../services/acccountService';
class Profile extends React.Component<RouteComponentProps> {
    state = {
        posts: [] as any,
        hasMoreItems: true,
        nextParams: {
            take: 5,
            skip: 0
        },
        user: { acessToken: '' },
        targetUserId: '',
        profileData: {
            postCount: 0,
            username: ''
        }
    };
    initialLoadCompleted = false;
    async componentDidMount() {
        var user = getUser();
        var targetUserId = this.props.match.params['id'];

        this.setState({ user: user, targetUserId: targetUserId });
        await this.fetchProfileData(targetUserId);
    }

    fetchPosts = async () => {
        var { nextParams, user, targetUserId } = this.state;
        var { data } = await getUserPosts(nextParams.skip, nextParams.take, targetUserId, user.acessToken);
        this.setState(
            {
                hasMoreItems: data.morePostsExist,
                posts: [...this.state.posts, ...data.posts],
                nextParams: {
                    skip: this.state.nextParams.skip + 5,
                    take: 5
                }
            });
    }

    fetchProfileData = async (userId) => {
        var { data } = await getProfile(userId);

        this.setState({
            profileData: {
                username: data.username,
                postCount: data.postCount
            }
        });
    }

    handleScroll = async e => {
        var el = await e.currentTarget.getScrollElement();

        var hitTheBottom = el.scrollHeight - el.scrollTop == el.clientHeight;
        if (hitTheBottom) {
            await this.fetchPosts();
            this.setState({ tracks: [...this.state.posts, 1] })
        }
    }

    profileRedirect = (userId) => {
        this.props.history.push('profile/' + userId);
    }


    updatePostsState = (postId, likeStatus) => {
        var { posts } = this.state;

        var postIndex = posts.findIndex(x => x.postId === postId);

        var postsBefore = posts.slice(0, postIndex);
        var postsAfter = posts.slice(postIndex + 1, posts.length)

        var post = posts[postIndex];

        if (likeStatus) {
            post.likeCount++;
        } else {
            post.likeCount--;
        }

        post.userLikesPost = likeStatus;

        this.setState({ posts: [...postsBefore, post, ...postsAfter] });
    }

    likePost = async postId => {
        await likePost(postId, this.state.user.acessToken);
        this.updatePostsState(postId, true);
    }

    unlikePost = async postId => {
        await unLikePost(postId, this.state.user.acessToken);
        this.updatePostsState(postId, false);
    }

    render() {
        if (this.state.user.acessToken !== '' && !this.initialLoadCompleted) {
            this.initialLoadCompleted = true;
            this.fetchPosts();
        }
        var items = [] as any;
        this.state.posts.map((item, i) => {
            items.push(<IonCard key={i}>
                <IonItem>
                    <IonLabel>{item.authorName}</IonLabel>
                    <IonButton onClick={() => this.profileRedirect(item.authorId)} fill="outline" slot="end">Profile</IonButton>
                </IonItem>
                <IonCardContent>
                    {item.content}
                </IonCardContent>
                <div className="display-flex justifyEnd">
                    {item.userLikesPost
                        ? <FaHeart size={20} color={'red'} onClick={() => this.unlikePost(item.postId)}></FaHeart>
                        : <FaRegHeart onClick={() => this.likePost(item.postId)} size={20}></FaRegHeart>
                    }
                    <div>{item.likeCount}</div>
                </div>
            </IonCard>);
        })

        return (
            <div>
                <IonPage >
                    <IonHeader>
                        <IonToolbar>
                            <IonButtons slot="start">
                                <IonMenuButton />
                            </IonButtons>
                            <IonTitle>Tsm</IonTitle>
                        </IonToolbar>
                    </IonHeader>
                    <IonContent scrollEvents={true} onIonScrollEnd={this.handleScroll}>
                        <IonCard className="welcome-card">
                            <img src="/assets/shapes.svg" alt="" />
                            <IonCardHeader>
                                <div className="centerD">
                                    <IonCardTitle>{this.state.profileData.username}</IonCardTitle>
                                </div>
                            </IonCardHeader>
                        </IonCard>
                        <IonCard className="welcome-card">
                            <IonCardHeader>
                                <div className="centerD">
                                    <IonCardTitle>Check out users {this.state.profileData.postCount} posts</IonCardTitle>
                                </div>
                            </IonCardHeader>
                        </IonCard>
                        {items}
                    </IonContent>

                </IonPage>
            </div >

        );
    }
};

export default Profile;