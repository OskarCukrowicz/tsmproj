import {
  IonButtons,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonInput,
  IonButton
} from '@ionic/react';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom'
import './Home.css';
import { signUp } from '../services/acccountService';
class Register extends React.Component<RouteComponentProps> {
  state = {
    account: {
      firstName: '',
      lastName: '',
      password: '',
      email: ''
    }
  }

  handleSubmit = async e => {
    e.preventDefault();
    var inputs = { ...this.state.account };
    await signUp(inputs.firstName, inputs.lastName, inputs.email, inputs.password);
    this.props.history.push('/login');
  }
  handleChange = e => {
    const account = { ...this.state.account };
    account[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ account });
  }

  render() {
    const { account } = this.state;
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Tsm</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonCard className="welcome-card">
            <img src="/assets/shapes.svg" alt="" />
            <IonCardHeader>
              <div className="centerD">
                <IonCardTitle>Register to TSM</IonCardTitle>
              </div>
            </IonCardHeader>
          </IonCard>
        </IonContent>

        <IonContent>
          <form onSubmit={this.handleSubmit}>
            <IonList>
              <div className='display-flex'>
                <IonItem>
                  <IonLabel position='floating'>First Name</IonLabel>
                  <IonInput
                    value={account.firstName}
                    type="text"
                    name='firstName'
                    onIonChange={this.handleChange}
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel position='floating'>Last Name</IonLabel>
                  <IonInput
                    value={account.lastName}
                    type="text"
                    name='lastName'
                    onIonChange={this.handleChange}
                  ></IonInput>
                </IonItem>
              </div>
              <IonItem>
                <IonLabel position='floating'>Email</IonLabel>
                <IonInput
                  type="text"
                  value={account.email}
                  name='email'
                  onIonChange={this.handleChange}
                ></IonInput>
              </IonItem>
              <IonItem>
                <IonLabel position='floating'>Password</IonLabel>
                <IonInput
                  value={account.password}
                  type="password"
                  name='password'
                  onIonChange={this.handleChange}
                ></IonInput>
              </IonItem>
              <div className='display-flex justifyEnd'>
                <IonButton type='submit'>Sign Up</IonButton>
              </div>
            </IonList>
          </form>
        </IonContent>

      </IonPage>
    );
  }

};

export default Register;
