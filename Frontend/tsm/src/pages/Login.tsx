import {
  IonButtons,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonInput,
  IonButton
} from '@ionic/react';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom'
import './Home.css';
import { signIn } from '../services/acccountService';
class Login extends React.Component<RouteComponentProps> {
  state = {
    account: {
      password: '',
      email: ''
    }
  }

  handleSubmit = async e => {
    e.preventDefault();
    var inputs = { ...this.state.account };
    var { data } = await signIn(inputs.email, inputs.password);
    localStorage.setItem('token', JSON.stringify(data.token));

    this.props.history.push('/main');
  }
  handleChange = e => {
    const account = { ...this.state.account };
    account[e.currentTarget.name] = e.currentTarget.value;

    this.setState({ account });
  }

  render() {
    const { account } = this.state;
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Tsm</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonCard className="welcome-card">
            <img src="/assets/shapes.svg" alt="" />
            <IonCardHeader>
              <div className="centerD">
                <IonCardTitle>Sign In to TSM</IonCardTitle>
              </div>
            </IonCardHeader>
          </IonCard>
        </IonContent>

        <IonContent>
          <form onSubmit={this.handleSubmit}>
            <IonList>
              <IonItem>
                <IonLabel position='floating'>Email</IonLabel>
                <IonInput
                  type="text"
                  value={account.email}
                  name='email'
                  onIonChange={this.handleChange}
                ></IonInput>
              </IonItem>
              <IonItem>
                <IonLabel position='floating'>Password</IonLabel>
                <IonInput
                  value={account.password}
                  type="password"
                  name='password'
                  onIonChange={this.handleChange}
                ></IonInput>
              </IonItem>
              <div className='display-flex justifyEnd'>
                <IonButton type='submit'>Sign In</IonButton>
              </div>
            </IonList>
          </form>
        </IonContent>

      </IonPage>
    );
  }
};

export default Login;
