import {
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton
} from '@ionic/react';
import { book, build, colorFill, grid, logIn, create } from 'ionicons/icons';
import React from 'react';
import './Home.css';

class HomePage extends React.Component {
  state = {
    deferredPrompt: {} as any,
    showInstallationButton: false
  }

  componentDidMount() {
    window.addEventListener('beforeinstallprompt', e => {
      e.preventDefault();
      this.setState({ deferredPrompt: e, showInstallationButton: true })
    });
  }

  installApp = () => {
    this.state.deferredPrompt.prompt();
    this.state.deferredPrompt.userChoice
      .then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          this.setState({ showInstallationButton: false })
        } else {
        }
      });
  }

  render() {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton />
            </IonButtons>
            <div className="display-flex">
              <IonTitle>Tsm</IonTitle>
              {
                this.state.showInstallationButton
                  ? <IonButton type='submit' onClick={this.installApp}>Install our App</IonButton>
                  : <div></div>
              }

            </div>

          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonCard className="welcome-card">
            <img src="/assets/shapes.svg" alt="" />
            <IonCardHeader>
              <div className="centerD">
                <IonCardTitle>Welcome to Tsm</IonCardTitle>
              </div>
            </IonCardHeader>
          </IonCard>

          <IonList lines="none">
            <IonListHeader>
              <IonLabel>Actions</IonLabel>
            </IonListHeader>
            <IonItem href="/login" target="_blank">
              <IonIcon slot="start" color="medium" icon={logIn} />
              <IonLabel>SignIn</IonLabel>
            </IonItem>
            <IonItem href="/register" target="_blank">
              <IonIcon slot="start" color="medium" icon={create} />
              <IonLabel>SignUp</IonLabel>
            </IonItem>
          </IonList>
        </IonContent>
      </IonPage>
    );
  }
};

export default HomePage;
