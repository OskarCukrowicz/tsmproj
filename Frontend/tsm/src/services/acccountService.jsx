import http from "./httpService"
import api from '../config.json'

const apiEndpoint = api.apiEndpoint + "/account";

export const signUp = (firstName, lastName, email, password) => {
    return http.post(apiEndpoint + '/register', {
        FirstName: firstName,
        LastName: lastName,
        Email: email,
        Password: password
    })
}

export const signIn = (email, password) => {
    return http.post(apiEndpoint + '/login', {
        Email: email,
        Password: password
    });
}


export const getProfile = (userId) => {
    return http.post(apiEndpoint + '/GetProfile', {
        UserId: userId
    });
}
