

export const getUser = () => {
    var jsonToken = localStorage.getItem('token');

    if (jsonToken == null) {
        return {
            dataPresent: false
        }
    }

    var token = JSON.parse(jsonToken);
    var currentEpochDate = new Date().valueOf();
    return {
        dataPresent: true,
        isExpired: currentEpochDate > token.expr,
        email: token.claims["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"],
        id: token.claims["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"],
        acessToken: token.accessToken
    }
}