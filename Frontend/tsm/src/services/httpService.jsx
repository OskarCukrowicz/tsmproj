import axios from 'axios';


//Just so code depends on the interface, not on axios itself
export default {
    get: axios.get,
    post: axios.post,
    put: axios.put,
    delete: axios.delete
}