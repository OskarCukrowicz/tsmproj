import http from "./httpService";
import api from '../config.json'

const apiEndpoint = api.apiEndpoint + "/posts";

export const getRecent = (from, take, token) => {
    return http.post(apiEndpoint + '/recent',
        {
            From: from,
            Take: take
        }, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
}

export const getUserPosts = (from, take, userId, token) => {
    return http.post(apiEndpoint + '/userPosts',
        {
            UserId: userId,
            From: from,
            Take: take
        }, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
}

export const likePost = (postId, token) => {
    return http.post(apiEndpoint + '/Like',
        {
            PostId: postId
        }, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
}

export const unLikePost = (postId, token) => {
    return http.post(apiEndpoint + '/UnLike',
        {
            PostId: postId
        }, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
}

export const addPost = (content, token) => {
    return http.post(apiEndpoint + '/Add',
        {
            Content: content
        }, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
}