Param([string] $imageTag)
 
if ([string]::IsNullOrEmpty($imageTag)) {
    $imageTag =  $(git rev-parse --abbrev-ref HEAD)
}

Write-Host "Building images with tag $imageTag" -ForegroundColor Yellow 

Push-Location -Path ../Backend
docker build -t oskarc/tsm:$imageTag . 
Pop-Location