﻿using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests.Builders
{
    public class PostBuilder : Builder<PostBuilder, SocialMediaTsm.Models.Database.Tables.Post>
    {
        public PostBuilder(SocialMediaContext context) : base(context)
        {
        }

        public PostBuilder WithContent(string content)
        {
            State.Content = content;
            return this;
        }

        public PostBuilder WithAuthor(User author)
        {
            State.Author = author;
            return this;
        }

        public PostBuilder AsReplyTo(SocialMediaTsm.Models.Database.Tables.Post post)
        {
            State.ReplyingTo = post;
            return this;
        }

    }
}
