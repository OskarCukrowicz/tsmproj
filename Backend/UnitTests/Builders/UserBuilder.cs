﻿using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests.Builders
{
    public class UserBuilder : Builder<UserBuilder, User>
    {
        public UserBuilder(SocialMediaContext context) : base(context)
        {
        }

        public UserBuilder WithFollowers(params User[] followers)
        {
            foreach(var follower in followers)
            {
                State.Followers.Add(new Follow()
                {
                    DateOfFollow = DateTime.UtcNow,
                    Follower = follower,
                });
            }

            return this;
        }
        public UserBuilder WithFollowing(params User[] followings)
        {
            foreach (var following in followings)
            {
                State.Followers.Add(new Follow()
                {
                    DateOfFollow = DateTime.UtcNow,
                    Following = following,
                });
            }

            return this;
        }

        public UserBuilder WithDescription(string description)
        {
            State.Description = description;
            return this;
        }

        public UserBuilder WithFullName(string firstName, string lastName)
        {
            State.FirstName = firstName;
            State.LastName = lastName;

            return this;
        }
    }
}
