﻿using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests.Builders
{
    public class ContextBuilder
    {
        public SocialMediaContext BuildClean()
        {
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<SocialMediaContext>();

            dbContextOptionsBuilder
                .EnableSensitiveDataLogging()
                .UseInMemoryDatabase(Guid.NewGuid().ToString());

            var databaseContext =
                new SocialMediaContext(dbContextOptionsBuilder.Options);
            databaseContext.Database.EnsureCreated();

            return databaseContext;
        }
    }
}
