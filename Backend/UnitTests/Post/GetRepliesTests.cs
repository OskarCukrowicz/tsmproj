﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnitTests.Builders;
using Xunit;
using SocialMediaTsm.Features.Posts.GetReplies;
using System.Threading;
using System.Linq;

namespace UnitTests.Post
{
    public class GetRepliesTests
    {
        [Fact]
        public async Task ShouldGetReplies()
        {
            var context = new ContextBuilder().BuildClean();
            var author = new UserBuilder(context).BuildAndSave();

            var mainPost = new PostBuilder(context)
                .WithAuthor(author)
                .WithContent("MAINPOST")
                .BuildAndSave();

            var reply = new PostBuilder(context)
                .WithAuthor(author)
                .WithContent("REPLYPOST")
                .AsReplyTo(mainPost)
                .BuildAndSave();

            var query = new Query
            {
                From = 0,
                Take = 1,
                PostId = mainPost.Id
            };

            var result = await new Handler(context).Handle(query, default(CancellationToken));

            Assert.Single(result.Replies);
            Assert.Equal(mainPost.Content, result.Post.Content);
            Assert.Equal(reply.Content, result.Replies.First().Content);
        }
    }
}
