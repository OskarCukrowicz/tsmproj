﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Features.Posts.Add;
using UnitTests.Builders;
using Xunit;

namespace UnitTests.Post
{
    public class AddPostTests
    {
        [Fact]
        public async Task ShouldAddPost()
        {
            var context = new ContextBuilder().BuildClean();

            var user = new UserBuilder(context).BuildAndSave();

            var command = new Command()
            {
                Content = "Creative Post",
                InReplyTo = 0,
                UserId = user.Id
            };

            var handler = new Handler(context);
            await handler.Handle(command, default(CancellationToken));

            var post = context.Posts.Include(x => x.Author).FirstOrDefault();

            Assert.Equal(command.Content, post.Content);
            Assert.Equal(user.Id, post.Author.Id);
        }

        [Fact]
        public async Task ShouldAddPostAsReply()
        {
            var context = new ContextBuilder().BuildClean();

            var user = new UserBuilder(context).BuildAndSave();
            var mainPost = new PostBuilder(context).BuildAndSave();
            var command = new Command()
            {
                Content = "Creative Post",
                InReplyTo = mainPost.Id,
                UserId = user.Id
            };

            var handler = new Handler(context);
            var result = await handler.Handle(command, default(CancellationToken));

            var post = context.Posts.Include(x => x.Author).FirstOrDefault(x=>x.Id == result.PostId);

            Assert.Equal(command.Content, post.Content);
            Assert.Equal(user.Id, post.Author.Id);

            Assert.Equal(command.InReplyTo, post.ReplyingToId);
        }
    }
}
