﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitTests.Builders;
using Xunit;
using SocialMediaTsm.Features.Posts.Edit;
using System.Threading;
using System.Threading.Tasks;

namespace UnitTests.Post
{
    public class EditPostTests
    {

        [Fact]
        public async Task ShoudlEditPost()
        {
            var context = new ContextBuilder().BuildClean();

            var user = new UserBuilder(context)
                .BuildAndSave();
            var post = new PostBuilder(context)
                .WithContent("content")
                .WithAuthor(user)
                .BuildAndSave();

            var command = new Command()
            {
                Content = "newContent",
                PostId = post.Id,
                UserId = user.Id
            };

            await new Handler(context).Handle(command, default(CancellationToken));

            var afterEdit = context.Posts.Find(post.Id);

            Assert.Equal(command.Content, afterEdit.Content);
        }

        [Fact]
        public async Task ShouldNotEditPostIfUserDoesNotOwnPost()
        {
            var context = new ContextBuilder().BuildClean();

            var author = new UserBuilder(context)
                .BuildAndSave();

            var caller = new UserBuilder(context)
                .BuildAndSave();
            var post = new PostBuilder(context)
                .WithContent("content")
                .WithAuthor(author)
                .BuildAndSave();

            var command = new Command()
            {
                Content = "newContent",
                PostId = post.Id,
                UserId = caller.Id
            };

            await Assert.ThrowsAsync<EditPostException>
                (async ()=>await new Handler(context).Handle(command, default(CancellationToken)));

            var afterEdit = context.Posts.Find(post.Id);

            Assert.NotEqual(command.Content, afterEdit.Content);
        }
    }
}
