﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.Like
{
    public class Command : IRequest<bool>
    {
        public int PostId { get; set; }
        public string UserId { get; set; }
    }

    public class Handler : BaseHandler<Command, bool>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<bool> Handle(Command request, CancellationToken cancellationToken)
        {
            var post = await Context.Posts.Include(x=>x.Likes)
                .FirstOrDefaultAsync(x => x.Id == request.PostId, cancellationToken);

            var likingAcc = await Context.Users
                .FirstOrDefaultAsync(x => x.Id == request.UserId, cancellationToken);

            var like = new Models.Database.Tables.Like()
            {
                Author = likingAcc,
                Post = post
            };

            post.Likes.Add(like);
            post.LikeCount += 1;

            await Context.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
