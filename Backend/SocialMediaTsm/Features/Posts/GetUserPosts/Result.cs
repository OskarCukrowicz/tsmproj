﻿using SocialMediaTsm.Features.Posts.GetReplies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.GetUserPosts
{
    public class Result
    {
        public List<PostDto> Posts { get; set; }
    }
}
