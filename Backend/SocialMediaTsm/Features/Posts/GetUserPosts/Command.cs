﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Features.Posts.GetReplies;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.GetUserPosts
{
    public class Query : IRequest<Result>
    {
        public string UserId { get; set; }
        public string CallerId { get; set; }
        public int From { get; set; }
        public int Take { get; set; }
    }

    public class Handler : BaseHandler<Query, Result>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<Result> Handle(Query request, CancellationToken cancellationToken)
        {
            var likes = Context.Likes.ToList();
            var posts = (await Context.Posts
                .Include(x => x.Author)
                .Include(x=>x.Likes)
                .OrderByDescending(x => x.DateOfCreation)
                .Skip(request.From)
                .Take(request.Take)
                .Where(x => x.Author.Id == request.UserId)
                .ToListAsync(cancellationToken)
                )
            .Select(x=>new PostDto(x, request.CallerId))
            .ToList();


            return new Result()
            {
                Posts = posts
            };
        }
    }
}
