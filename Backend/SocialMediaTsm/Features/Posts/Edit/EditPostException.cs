﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.Edit
{
    public class EditPostException : Exception
    {
        public EditPostException(string message) : base(message)
        {
        }
    }
}
