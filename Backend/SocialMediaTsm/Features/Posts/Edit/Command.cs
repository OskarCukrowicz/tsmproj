﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.Edit
{
    public class Command : IRequest<bool>
    {
        public int PostId { get; set; }
        public string UserId { get; set; }
        public string Content { get; set; }
    }

    public class Handler : BaseHandler<Command, bool>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<bool> Handle(Command request, CancellationToken cancellationToken)
        {
            var post = await Context.Posts
                .Include(x => x.Author)
                .Where(x => x.Id == request.PostId)
                .Where(x => x.Author.Id == request.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            if(post is null)
            {
                throw new EditPostException("Post does not exist, or calling user is not an author of the post");
            }

            post.Content = request.Content;

            await Context.SaveChangesAsync(cancellationToken);

            return true;
        }
    }

}
