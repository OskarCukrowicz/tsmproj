﻿using SocialMediaTsm.Features.Posts.GetReplies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.Recent
{
    public class Result
    {
        public List<PostDto> Posts { get; set; }
        public bool MorePostsExist { get; set; }
    }
}
