﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.Recent
{
    public class Query : IRequest<Result>
    {
        public int From { get; set; }
        public int Take { get; set; }
        public string CallingUserId { get; set; }
    }

    public class Handler : BaseHandler<Query, Result>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<Result> Handle(Query request, CancellationToken cancellationToken)
        {
            var posts = await Context.Posts.Include(x => x.Author)
                .Include(x=>x.Likes)
                .OrderByDescending(x => x.DateOfCreation)
                .Skip(request.From)
                .Take(request.Take)
                .ToListAsync();

            return new Result()
            {
                MorePostsExist = posts.Count == request.Take,
                Posts = posts.Select(x => new GetReplies.PostDto(x, request.CallingUserId)).ToList()
            };
        }
    }
}
