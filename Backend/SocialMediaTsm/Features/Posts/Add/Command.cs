﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Features.Posts.GetReplies;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using SocialMediaTsm.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.Add
{
    public class Command : IRequest<PostDto>
    {
        public string UserId { get; set; }
        public string Content { get; set; }
        public int InReplyTo { get; set; }
    }

    public class Handler : BaseHandler<Command, PostDto>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<PostDto> Handle(Command request, CancellationToken cancellationToken)
        {
            var user = await Context.Users
                .Where(x => x.Id == request.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            var post = new Post()
            {
                Content = request.Content,
                DateOfCreation = DateTime.UtcNow,
                LikeCount = 0,
                Author = user
            };

            Context.Posts.Add(post);
            await Context.SaveChangesAsync(cancellationToken);

            return new PostDto(post, request.UserId);
        }
    }

}
