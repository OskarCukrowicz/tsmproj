﻿using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.GetReplies
{
    public class Result
    {
        public PostDto Post { get; set; }
        public List<PostDto> Replies { get; set; }
    }

    public class PostDto
    {
        public string AuthorId { get; set; }
        public string Content { get; set; }
        public int PostId { get; set; }
        public int LikeCount { get; set; }
        public DateTime DateOfCreation { get; set; }
        public string AuthorName { get; set; }
        public bool UserLikesPost { get; set; }
        public PostDto(Post post, string callingUserId)
        {
            AuthorId = post.Author.Id;
            Content = post.Content;
            PostId = post.Id;
            LikeCount = post.LikeCount;
            DateOfCreation = post.DateOfCreation;
            AuthorName = $"{post.Author.FirstName} {post.Author.LastName}";
            UserLikesPost = post.Likes.Select(x => x.AuthorId).Contains(callingUserId);
        }
    }
}
