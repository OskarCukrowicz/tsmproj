﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.UnLikePost
{
    public class Command : IRequest<bool>
    {
        public string UserId { get; set; }
        public int PostId { get; set; }
    }

    public class Handler : BaseHandler<Command, bool>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<bool> Handle(Command request, CancellationToken cancellationToken)
        {
            var like = Context.Likes.Include(x => x.Post).FirstOrDefault(x => x.PostId == request.PostId && x.AuthorId == request.UserId);

            like.Post.LikeCount -= 1;
            Context.Posts.Update(like.Post);

            Context.Remove(like);
            Context.SaveChanges();

            return true;
        }
    }
}
