﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Posts.Remove
{
    public class Command : IRequest<bool>
    {
        public int PostId { get; set; }
        public string UserId { get; set; }
    }

    public class Handler : BaseHandler<Command, bool>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<bool> Handle(Command request, CancellationToken cancellationToken)
        {
            var post = await Context.Posts
                .Include(x => x.Author)
                .Where(x => x.Id == request.PostId)
                .Where(x => x.Author.Id == request.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            Context.Posts.Remove(post);
            Context.SaveChanges();

            return true;
        }
    }
}
