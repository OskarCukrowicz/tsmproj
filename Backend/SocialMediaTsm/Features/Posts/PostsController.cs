﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Extensions;

namespace SocialMediaTsm.Features.Posts
{
    [Authorize(ActiveAuthenticationSchemes = "Bearer")]
    public class PostsController : BaseController
    {
        public PostsController(IMediator mediator, SocialMediaContext context) : base(mediator)
        {
        }

        [HttpPost]
        public async Task<ActionResult<Add.Result>> Add(Add.Command command)
        {
            command.UserId = User.GetId();
            var result = await Mediator.Send(command);

            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Edit.Command command)
        {
            var result = await Mediator.Send(command);

            return Ok(result);
        }



        [HttpPost]
        public async Task<IActionResult> Recent([FromBody]Recent.Query query)
        {
            query.CallingUserId = User.GetId();
            var result = await Mediator.Send(query);

            return Ok(result);
        }


        [HttpPost]
        public async Task<ActionResult<GetUserPosts.Result>> UserPosts(GetUserPosts.Query query)
        {
            query.CallerId = User.GetId();
            var result = await Mediator.Send(query);

            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> Like(Like.Command command)
        {
            command.UserId = User.GetId();

            await Mediator.Send(command);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> UnLike(UnLikePost.Command command)
        {
            command.UserId = User.GetId();

            await Mediator.Send(command);

            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> Remove(Remove.Command command)
        {
            await Mediator.Send(command);

            return Ok();
        }
    }
}