﻿using MediatR;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Follow.Follow
{
    public class Command : IRequest<bool>
    {
        public string FollowerId { get; set; }
        public string FollowingId { get; set; }
    }

    public class Handler : BaseHandler<Command, bool>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<bool> Handle(Command request, CancellationToken cancellationToken)
        {
            var follow = new Models.Database.Tables.Follow()
            {
                DateOfFollow = DateTime.UtcNow,
                FollowerId = request.FollowerId,
                FollowingId = request.FollowingId
            };

            Context.Follows.Add(follow);
            Context.SaveChanges();

            return true;
        }
    }

}
