﻿using MediatR;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Follow.GetFollowers
{
    public class Query : IRequest<Result>
    {
        public string UserId { get; set; }
    }

    public class Handler : BaseHandler<Query, Result>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<Result> Handle(Query request, CancellationToken cancellationToken)
        {
            var followers = Context.Follows.Where(x => x.FollowingId == request.UserId)
                .Select(x=>x.FollowerId)
                .ToList();

            var followersDto = Context.Users
                .Where(x => followers.Contains(x.Id))
                .Select(x => new UserDto() { FullName = x.UserName, UserId = x.Id }).ToList();

            return new Result()
            {
                Followers = followersDto
            };
        }
    }

}
