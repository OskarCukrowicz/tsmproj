﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Follow.GetFollowers
{
    public class Result
    {
        public List<UserDto> Followers { get; set; }
    }

    public class UserDto
    {
        public string FullName { get; set; }
        public string UserId { get; set; }
    }
}
