﻿using MediatR;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Follow.UnFollow
{
    public class Command : IRequest<bool>
    {
        public string FollowerId { get; set; }
        public string FollowingId { get; set; }
    }

    public class Handler : BaseHandler<Command, bool>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<bool> Handle(Command request, CancellationToken cancellationToken)
        {
            var follow = Context.Follows.FirstOrDefault(x => x.FollowerId == request.FollowerId && x.FollowerId == request.FollowingId);

            Context.Follows.Remove(follow);
            Context.SaveChanges();

            return true;
        }
    }
}
