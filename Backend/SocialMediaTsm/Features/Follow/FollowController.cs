﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialMediaTsm.Models.BaseClasses;
using Swashbuckle.AspNetCore.Annotations;

namespace SocialMediaTsm.Features.Follow
{
    public class FollowController : BaseController
    {
        public FollowController(IMediator mediator) : base(mediator)
        {
        }


        [HttpPost]
        public async Task<IActionResult> Follow(Follow.Command command)
        {
            var result = await Mediator.Send(command);

            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult<GetFollowers.Result>> GetFollowers(GetFollowers.Query query)
        {
            var result = await Mediator.Send(query);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> UnFollow(UnFollow.Command command)
        {
            await Mediator.Send(command);

            return Ok();
        }
    }
}