﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Account.RequestResetPassword
{
    public class Command
    {
        public string Email { get; set; }
    }
}
