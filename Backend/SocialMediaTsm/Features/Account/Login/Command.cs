﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SocialMediaTsm.Infrastructure.Identity;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Account.Login
{
    public class Command : IRequest<LoginResult>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }


    public class Handler : IRequestHandler<Command, LoginResult>
    {
        private readonly SignInManager<User> _signInManager;
        private readonly IClaimsProvider _claimsProvider;
        private readonly IJwtHandler _jwtHandler;

        public Handler(SignInManager<User> signInManager, IClaimsProvider claimsProvider, IJwtHandler jwtHandler)
        {
            _signInManager = signInManager;
            _claimsProvider = claimsProvider;
            _jwtHandler = jwtHandler;
        }

        public async Task<LoginResult> Handle(Command cmd, CancellationToken cancellationToken)
        {
            var user = await _signInManager.UserManager.FindByEmailAsync(cmd.Email);
            if (user == null)
            {
                throw new LoginException("Wrong password or email");
            }


            var resultPassword = await _signInManager.CheckPasswordSignInAsync(user, cmd.Password, lockoutOnFailure: false);
            if (!resultPassword.Succeeded)
            {
                throw new LoginException("Wrong password or email");
            }

            await _signInManager.SignInAsync(user, isPersistent: false);
            var claims = await _claimsProvider.GetAsync(user.Id);
            var jwt = _jwtHandler.CreateToken(Guid.Parse(user.Id).ToString(), user.Role, claims);


            return new LoginResult(jwt);
        }
    }

    public class LoginResult
    {
        public JsonWebToken Token { get; set; }
        public bool Suceeded { get; set; }

        public LoginResult()
        {

        }
        public LoginResult(JsonWebToken token)
        {
            Token = token;
            Suceeded = true;
        }
    }
}


