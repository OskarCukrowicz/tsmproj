﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using SocialMediaTsm.Infrastructure.Identity;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using SocialMediaTsm.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Account.Register
{
    public class Command : IRequest<string>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
    public class Handler : BaseHandler<Command, string>
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly UserManager<User> _userManager;

        public Handler(UserManager<User> userManager, RoleManager<Role> roleManager,
            SocialMediaContext context) :
            base(context)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public override async Task<string> Handle(Command request,
            CancellationToken cancellationToken = default(CancellationToken))
        {

            var user = UserAggregate.NewUser();

            user.UpdateName(request.FirstName, request.LastName);
            user.UpdateEmail(request.Email);

            var userState = user.GetState();

            IdentityResult identityResult = await _userManager.CreateAsync(userState, request.Password);
            if (!identityResult.Succeeded)
            {
                throw new CreateUserException(identityResult.Errors.Select(x => x.Description).FirstOrDefault());
            }


            Role role = _roleManager.Roles.FirstOrDefault(x => x.Name == Roles.User);
            if (role == null)
            {
                throw new CreateUserException("Role does not exist");
            }
            Context.Attach(role);

            await Context.SaveChangesAsync(cancellationToken);

            await _userManager.AddToRoleAsync(userState, role.Name);
            userState.Role = role;
            await _userManager.UpdateAsync(userState);

            return userState.Id;
        }
    }

    public class CreateUserException : Exception
    {
        public CreateUserException(string msg) : base(msg)
        {
        }
    }
}
