﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Account.GetProfile
{
    public class Result
    {
        public string Username { get; set; }
        public int PostCount { get; set; }
    }
}
