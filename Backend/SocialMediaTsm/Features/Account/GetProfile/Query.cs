﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Models.BaseClasses;
using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Features.Account.GetProfile
{
    public class Query : IRequest<Result>
    {
        public string UserId { get; set; }
    }


    public class Handler : BaseHandler<Query, Result>
    {
        public Handler(SocialMediaContext context) : base(context)
        {
        }

        public override async Task<Result> Handle(Query request, CancellationToken cancellationToken)
        {
            return Context.Users.Include(x => x.Posts).Where(x => x.Id == request.UserId).Select(x => new Result()
            {
                PostCount = x.Posts.Count,
                Username = $"{x.FirstName} {x.LastName}"
            }).FirstOrDefault();         
        }
    }
}
