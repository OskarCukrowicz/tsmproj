﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialMediaTsm.Features.Account.Login;
using SocialMediaTsm.Features.Account.RenameResetPassword;
using SocialMediaTsm.Models.BaseClasses;

namespace SocialMediaTsm.Features.Account
{
    public class AccountController : BaseController
    {
        public AccountController(IMediator mediator) : base(mediator)
        {
        }

        [HttpPost]
        public async Task<ActionResult<LoginResult>> Login([FromBody]Login.Command command, CancellationToken token)
        {
            var result = await Mediator.Send(command, token);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]Register.Command command, CancellationToken token)
        {
            await Mediator.Send(command, token);

            return Ok();
        }
        [HttpPost]
        public async Task<ActionResult<Result>> RequestResetPassword([FromBody] RequestResetPassword.Command cmd, CancellationToken cancellationToken)
        {

            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> ResetPassword([FromBody] ResetPassword.Command cmd, CancellationToken cancellationToken)
        {

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> GetProfile(GetProfile.Query query)
        {
            var result = await Mediator.Send(query);

            return Ok(result);
        }
    }
}