﻿using Microsoft.AspNetCore.Identity;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.DataSeeders
{
    public class UserDataSeeder : AbstractDataSeeder
    {
        private readonly UserManager<User> _userManager;

        private List<User> _users = new List<User>()
        {
            new User()
            {
                Id = Guid.NewGuid().ToString(),
                Email = "seeded@email.com",
                FirstName = "Oskar",
                LastName = "Testowy"
            },
            new User()
            {
                Id = Guid.NewGuid().ToString(),
                Email = "seeded2@email.com",
                FirstName = "Wojciech",
                LastName = "Testowy"
            },
            new User()
            {
                Id = Guid.NewGuid().ToString(),
                Email = "seeded2@email.com",
                FirstName = "Tomasz",
                LastName = "Testowy"
            },
        };

        public UserDataSeeder(SocialMediaContext dbContext, UserManager<User> userManager) : base(dbContext)
        {
            _userManager = userManager;
        }

        public override bool DataExists()
        {
            return Context.Users.Any();
        }

        public override async Task SeedData()
        {
            var role = Context.Roles.FirstOrDefault(x => x.Name == Roles.User);

            foreach(var user in _users)
            {
                user.Role = role;
                await _userManager.CreateAsync(user, "Qwer123$");
            }
        }
    }
}
