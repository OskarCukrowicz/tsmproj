﻿using Microsoft.AspNetCore.Identity;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.DataSeeders
{
    public class DataSeeder
    {
        private List<AbstractDataSeeder> _seeders = new List<AbstractDataSeeder>();
        public DataSeeder(SocialMediaContext context, RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            _seeders.Add(new RoleDataSeeder(context, roleManager));
            _seeders.Add(new UserDataSeeder(context, userManager));
            _seeders.Add(new PostDataSeeder(context));
        }

        public async Task SeedData()
        {
            foreach(var seeder in _seeders)
            {
                await seeder.SeedData();
            }
        }
    }
}
