﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;

namespace SocialMediaTsm.Models.DataSeeders
{
    public class PostDataSeeder : AbstractDataSeeder
    {
        public PostDataSeeder(SocialMediaContext context) : base(context)
        {
        }

        public override bool DataExists()
        {
            return Context.Posts.Any();
        }

        public override async Task SeedData()
        {
            var users = Context.Users.ToList();

            var random = new Random();
            foreach(var user in users)
            {
                for(int i = 0; i < 3; i++)
                {
                    user.Posts.Add(new Post()
                    {
                        DateOfCreation = DateTime.UtcNow.AddMinutes(-random.Next(6000)),
                        LikeCount = 0,
                        Content = $"Seeded Content {random.Next(50)}"
                    });
                }              
            }

            await Context.SaveChangesAsync();
        }
    }
}
