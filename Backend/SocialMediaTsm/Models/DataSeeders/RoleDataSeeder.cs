﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SocialMediaTsm.Infrastructure.Identity;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;

namespace SocialMediaTsm.Models.DataSeeders
{
    public class RoleDataSeeder : AbstractDataSeeder
    {
        private RoleManager<Role> _roleManager { get; set; }
        public RoleDataSeeder(SocialMediaContext context, RoleManager<Role> roleManager) : base(context)
        {
            _roleManager = roleManager;
        }

        public override bool DataExists()
        {
            return false;
        }

        public override async Task SeedData()
        {
            var seedRoles = new List<SeedRole>
            {
                new SeedRole()
                {
                    Role = new Role(Roles.User),
                    Claims = RoleClaims.User
                },
                new SeedRole()
                {
                    Role = new Role(Roles.Admin),
                    Claims = RoleClaims.Admin
                }
            };

            foreach (var seedRole in seedRoles)
            {
                bool roleExist = await _roleManager.RoleExistsAsync(seedRole.Role.Name);
                if (roleExist)
                {
                    var role = await _roleManager.FindByNameAsync(seedRole.Role.Name);
                    var claims = await _roleManager.GetClaimsAsync(role);

                    foreach (var claim in claims)
                    {
                        await _roleManager.RemoveClaimAsync(role, claim);
                    }

                    foreach (var claim in seedRole.Claims)
                    {
                        await _roleManager.AddClaimAsync(role, claim);
                    }
                }
                else
                {
                    await _roleManager.CreateAsync(seedRole.Role);
                    var role = await _roleManager.FindByNameAsync(seedRole.Role.Name);
                    foreach (var claim in seedRole.Claims)
                    {
                        await _roleManager.AddClaimAsync(role, claim);
                    }
                }
            }
        }
    }

    public class SeedRole
    {
        public Role Role { get; set; }
        public List<Claim> Claims { get; set; }
    }
}
