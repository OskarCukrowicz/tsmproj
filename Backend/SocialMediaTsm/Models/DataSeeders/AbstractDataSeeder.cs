﻿using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.DataSeeders
{
    public abstract class AbstractDataSeeder
    {
        protected SocialMediaContext Context { get; set; }

        public AbstractDataSeeder(SocialMediaContext context)
        {
            Context = context;
        }

        public abstract bool DataExists();
        public abstract Task SeedData();
    }
}
