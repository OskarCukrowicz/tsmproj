﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.DomainExceptions
{
    public class UserAggregateException : Exception
    {
        public UserAggregateException(string message) : base(message)
        {
        }
    }
}
