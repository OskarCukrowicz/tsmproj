﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Repositories
{
    public abstract class Repository<T>
        where T : Entity
    {
        protected SocialMediaContext Context { get; set; }
        public Repository(SocialMediaContext context)
        {
            Context = context;
        }

        public async Task<T> GetById(int id, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            IQueryable<T> query = Context.Set<T>();
            if (include != null)
            {
                query = include(query);
            }

            T entity = await query.FirstOrDefaultAsync(x => x.Id == id);
            if (entity is null)
            {
                throw new GetByIdException($"Entry of type {nameof(T)} with id {id} does not exist");
            }

            return entity;
        }
    }
}
