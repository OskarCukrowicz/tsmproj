﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Repositories
{
    public class GetByIdException : Exception
    {
        public GetByIdException(string message) : base(message)
        {
        }
    }
}
