﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Domain
{
    public abstract class Aggregate<T>
        where T: class
    {
        protected T State { get; set; }

        public Aggregate(T state)
        {
            State = state;
        }

        public T GetState()
        {
            return State;
        }
    }
}
