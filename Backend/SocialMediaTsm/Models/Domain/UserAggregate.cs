﻿using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Domain
{
    public class UserAggregate : Aggregate<User>
    {
        public static UserAggregate NewUser()
        {
            return new UserAggregate(new User());
        }

        public UserAggregate(User user): base(user)
        {

        }

        public void UpdateName(string firstName, string lastName)
        {
            State.FirstName = firstName;
            State.LastName = lastName;
        }

        public void UpdateEmail(string email)
        {
            State.Email = email;
            State.UserName = email;
        }

        public void UpdateDescription(string description)
        {
            State.Description = description;
        }

        public void AddPost(Post post)
        {
            State.Posts.Add(post);
        }
    }
}
