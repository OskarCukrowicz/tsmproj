﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Database.Tables
{
    public class Follow : Entity
    {
        public User Follower { get; set; }
        public string FollowerId { get; set; }
        public User Following { get; set; }
        public string FollowingId {get;set;}
        public DateTime DateOfFollow { get; set; }
    }
}
