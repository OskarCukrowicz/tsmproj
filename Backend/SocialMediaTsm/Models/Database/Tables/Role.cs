﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Database.Tables
{
    public class Role : IdentityRole
    {
        public Role()
        {
        }

        public Role(string name) : base(name)
        {

        }
    }

    public static class Roles
    {
        public const string Admin = "admin";
        public const string User = "user";
    }
}
