﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Database.Tables
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime RegistrationDate { get; set; }
        public List<Post> Posts { get; set; }
        public Role Role { get; set; }
        public List<Follow> Followers { get; set; }
        public List<Follow> Following { get; set; }

        public User()
        {
            Followers = new List<Follow>();
            Following = new List<Follow>();
            Posts = new List<Post>();
        }
    }
}
