﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Database.Tables
{
    public class Like : Entity
    {
        public User Author { get; set; }
        public string AuthorId { get; set; }
        public Post Post { get; set; }
        public int PostId { get; set; }
    }
}
