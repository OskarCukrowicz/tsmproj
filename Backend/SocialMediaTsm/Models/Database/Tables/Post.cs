﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Database.Tables
{
    public class Post :Entity
    {
        public string Content { get; set; }
        public DateTime DateOfCreation { get; set; }
        public User Author { get; set; }
        public Post ReplyingTo { get; set; }
        public int? ReplyingToId { get; set; }
        public int LikeCount { get; set; }
        public List<Like> Likes { get; set; }


        public Post()
        {
            Likes = new List<Like>();
        }
    }
}
