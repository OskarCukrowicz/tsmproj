﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Models.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.Database
{
    public class SocialMediaContext : IdentityDbContext<User, Role, string>
    {
        public SocialMediaContext(DbContextOptions options) : base(options)
        {
            if (!Database.IsInMemory())
            {
                this.Database.Migrate();
            }
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Follow> Follows { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                .HasMany(x => x.Followers)
                .WithOne(x=>x.Following);

            builder.Entity<User>()
                .HasMany(x => x.Following)
                .WithOne(x => x.Follower);

            base.OnModelCreating(builder);
        }
    }
}
