﻿using MediatR;
using SocialMediaTsm.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocialMediaTsm.Models.BaseClasses
{
    public abstract class BaseHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse>
        where TRequest : IRequest<TResponse>

    {
        protected SocialMediaContext Context { get; set; }
        public BaseHandler(SocialMediaContext context)
        {
            Context = context;
        }

        public abstract Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken);
    }
}
