using System.Threading.Tasks;

namespace SocialMediaTsm.Infrastructure.Identity
{
    public interface IAccessTokenService
    {
        Task<bool> IsCurrentActiveToken();
        Task DeactivateCurrentAsync();
        Task<bool> IsActiveAsync(string token);
        Task DeactivateAsync(string token);
        string GetCurrent();
    }
}