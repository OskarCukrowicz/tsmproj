using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialMediaTsm.Models.Database;

namespace SocialMediaTsm.Infrastructure.Identity
{
    public class ClaimsProvider : IClaimsProvider
    {
        private readonly SocialMediaContext _context;

        public ClaimsProvider(SocialMediaContext context)
        {
            _context = context;
        }

        public async Task<IDictionary<string, string>> GetAsync(string userId)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(x => x.Id == userId);
            var dictionary = new Dictionary<string, string>();

            dictionary.Add(ClaimTypes.Email, user.Email);
            dictionary.Add(ClaimTypes.NameIdentifier, user.Id);

            return await Task.FromResult(dictionary);
        }
    }

    public interface IClaimsProvider
    {
        Task<IDictionary<string, string>> GetAsync(string userId);
    }
}