﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMediaTsm.Infrastructure.Identity
{
    public static class Policy
    {
        public static List<string> GetAllPermissions()
        {
            return typeof(Permissions).GetNestedTypes()
                .SelectMany(x => x.GetFields())
                .Select(x => x.GetValue(null))
                .Cast<string>()
                .ToList();
        }
    }
}
