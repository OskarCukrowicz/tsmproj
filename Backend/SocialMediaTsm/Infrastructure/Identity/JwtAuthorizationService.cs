using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.IdentityModel.JsonWebTokens;

namespace SocialMediaTsm.Infrastructure.Identity
{
    public interface IJwtAuthorizationService
    {
        bool IsAuthenticated();
        ClaimsPrincipal GetUserData();
    }

    public class JwtAuthorizationService : IJwtAuthorizationService
    {
        private readonly IJwtHandler _jwtHandler;
        private readonly IAccessTokenService _tokenService;

        public JwtAuthorizationService(IJwtHandler jwtHandler, IAccessTokenService tokenService)
        {
            _jwtHandler = jwtHandler;
            _tokenService = tokenService;
        }

        public bool IsAuthenticated()
        {
            try
            {
                var current = _tokenService.GetCurrent();
                var payload = _jwtHandler.GetTokenPayload(current);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public ClaimsPrincipal GetUserData()
        {
            try
            {
                string current = _tokenService.GetCurrent();
                JsonWebTokenPayload payload = _jwtHandler.GetTokenPayload(current);

                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.NameIdentifier, payload.Claims[ClaimTypes.NameIdentifier]),
                    new Claim(ClaimTypes.Name, payload.Claims[ClaimTypes.Name]),
                    new Claim(ClaimTypes.Email, payload.Claims[ClaimTypes.Email]),
                };
                
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims);
                return new ClaimsPrincipal(new List<ClaimsIdentity>() {claimsIdentity});
            }
            catch (Exception e)
            {
                return new ClaimsPrincipal();
            }
        }
    }
}