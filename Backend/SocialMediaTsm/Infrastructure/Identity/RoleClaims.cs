﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialMediaTsm.Infrastructure.Identity
{
    public static class RoleClaims
    {
        public const string ClaimType = "smp/permission";
        public static readonly List<Claim> User = new List<Claim>
        {
            CreateClaim(Permissions.Posts.Add)
        };

        public static readonly List<Claim> Admin = new List<Claim>
        {
        };

        private static Claim CreateClaim(string value)
        {
            return new Claim(ClaimType, value);
        }
    }
}
