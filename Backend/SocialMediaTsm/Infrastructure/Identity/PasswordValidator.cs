﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading.Tasks;


namespace SocialMediaTsm.Infrastructure.Identity
{
    public static class PasswordValidator
    {
        public static void Validate(string password, string confirmPassword)
        {
            if (password != confirmPassword)
            {
                throw new PasswordValidatorException("");
            }
        }
    }

    public class PasswordValidatorException : Exception
    {
        public PasswordValidatorException(string msg) : base(msg)
        {
        }
    }
}
