namespace SocialMediaTsm.Infrastructure.Identity
{
    public class JwtOptions
    {
        public string SecretKey { get; set; }
        public string Issuer { get; set; }
        public int ExpiryMinutes { get; set; }
        public bool ValidateLifetime { get; set; }
        public bool ValidateAudience { get; set; }
        public string ValidAudience { get; set; }

        public JwtOptions()
        {
            SecretKey = "JLBMU2VbJZmt42sUwByUpJJF6Y5mG2gPNU9sQFUpJFcGFJdyKxskR3bxh527kax2UcXHvB";
            ExpiryMinutes = 180;
            Issuer = "dshop-identity-service";
            ValidateLifetime = true;
        }
    }
}