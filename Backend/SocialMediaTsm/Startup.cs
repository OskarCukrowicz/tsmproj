﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using SocialMediaTsm.Infrastructure.Identity;
using SocialMediaTsm.Models.Database;
using SocialMediaTsm.Models.Database.Tables;
using SocialMediaTsm.Models.DataSeeders;
using Swashbuckle;
using Swashbuckle.AspNetCore.Swagger;

namespace SocialMediaTsm
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddCors();
            services.AddMvc()
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var connectionString = Configuration["ConnectionStrings:DefaultConnection"];
            services.AddDbContext<SocialMediaContext>(options =>
                options.UseMySql(connectionString)
            );
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<SocialMediaContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IClaimsProvider, ClaimsProvider>();
            services.AddJwt();
            services.AddAuthorization(options =>
            {
                foreach (var key in Policy.GetAllPermissions())
                {
                    options.AddPolicy(key,
                        policy => { policy.RequireClaim(RoleClaims.ClaimType, key); });
                }
            });
            services.AddMediatR(typeof(Startup));
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "TsmProjApi",
                });
                c.CustomSchemaIds(x => x.FullName);
                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };
                c.EnableAnnotations();
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
            });
            using (var serviceProvider = services.BuildServiceProvider())
            {
                var context = serviceProvider.GetService<SocialMediaContext>();
                var userManager = serviceProvider.GetService<UserManager<User>>();
                var roleManager = serviceProvider.GetService<RoleManager<Role>>();
                new DataSeeder(context, roleManager, userManager).SeedData().Wait();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(x=>x.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod());

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API");
            });
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Index}/{id?}");
            });
        }
    }
}
